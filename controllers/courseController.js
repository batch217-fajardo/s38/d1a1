const Course = require("../models/Course.js");
const auth = require("../auth.js");

// module.exports.addCourse = (reqBody) => {


// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then((newCourse, error) => {
// 		if (error){
// 			return false;
// 		}

// 	});

// }

module.exports.addCourse = (reqBody) => {
	if (auth.verify !== null){

		let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newCourse.save().then((newCourse, error) => {
		if (error){
			return false;
		} else {
			return true;
		}

	});

	} else {
		return false
	}
}